<div class="btn-group">
	  <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action</button>
    	  <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -2px, 0px);">
              
              	<form method="post" action="{{ $path }}/{{$id}}" >
                    @csrf()
                    {{ method_field('DELETE') }}
                    <button class="dropdown-item">Delete</button>
                </form>
              
              <a class="dropdown-item" href="{{$path}}/{{$id}}">Edit</a>
        	</div>
    
</div>