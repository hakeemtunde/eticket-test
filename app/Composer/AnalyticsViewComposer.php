<?php
namespace App\Composer;

use Illuminate\View\View;
use App\Ticket;
use App\ETicket\CommonStr;

class AnalyticsViewComposer
{

    public function compose(View $view)
    {
        $tickets = Ticket::all();
        $total_closed = $tickets->filter(function ($ticket) {
            return (strcmp($ticket->status, CommonStr::CLOSED_STATUS) == 0);
        });
        $total_pending = $tickets->filter(function ($ticket) {
            return (strcmp($ticket->status, CommonStr::PENDDING_STATUS) == 0);
        });

        $view->with([
            'total_closed' => $total_closed->count(),
            'total_pending' => $total_pending->count()
        ]);
    }
}

